SportLobster Test
========================

Codebase is on Symfony 2.5 standard edition with the new 3 directory structure.

1) Installation & Configuration
----------------------------------

# Clone the repo to your local computer and setup a virtual host (e.g. sport-lobster-test.localhost) to point to the /web directory.
# Run composer install to get all the required libraries.
# Update the URL parameter in the feed.url in app/config/config.yml with the URL where the news feed is available.
# Optionally change the default categories to hide and filter by. 
# Go to http://sport-lobster-test.localhost/app_dev.php/football to run!

Notes:

* The layout pulls in Twitter Bootstrap from a CDN so you need an internet connection.
* Tests are done via PHPUnit, simply run phpunit from the root directory.

2) Todo
-------------------------------------

* Move logic code from controller to a service
* Implement behat testing rather than PHPUnit
* Support a JSON data feed
* Add test cases:
	* Network error for loading the feed
	* Syntax error in loading feed
	* No articles
	* Linking to category page if category is Report
	* Converting the categories to hide and the available categories for filtering to arrays