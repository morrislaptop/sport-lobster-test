<?php

namespace SportLobster\FeedBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction($category = null)
    {
    	// Get the Feed Contents
    	$feedUrl = $this->container->getParameter('feed.url');
    	$client = $this->get('guzzle.client');
    	try {
    		$request = $client->get($feedUrl);
    		$response = $client->send($request);
    	} 
        catch (\Exception $e) 
        {
            $content = $this->renderView(
                'SportLobsterFeedBundle:Default:error.html.twig',
                array('reason' => 'Network')
            );
            $response = new Response($content);
            $response->setStatusCode(500);
            return $response;
		}

		// Parse
        try {
    	   $feed = $response->xml(); # @todo Cater for JSON data
        } 
        catch (\Exception $e) {
            $content = $this->renderView(
                'SportLobsterFeedBundle:Default:error.html.twig',
                array('reason' => 'Syntax')
            );
            $response = new Response($content);
            $response->setStatusCode(500);
            return $response;
        }

        // Check for articles
        if ( !$feed->channel->item ) {
            $content = $this->renderView(
                'SportLobsterFeedBundle:Default:error.html.twig',
                array('reason' => 'No articles')
            );
            $response = new Response($content);
            $response->setStatusCode(404);
            return $response;
        }

        // Get channel info
    	$title = $feed->channel->title;
    	$description = $feed->channel->description;
    	$topic = $feed->channel->category; # using $topic to not clash with item->category

    	// Get the news articles
    	if ( $category ) {
    		$news = $feed->xpath('//item[category="' . $category . '"]');
    	}
        else {
            $news = $feed->xpath('//item[category!="' . $this->container->getParameter('feed.hide_category') . '"]');
        }

    	// Display the view
        $filter_category = $this->container->getParameter('feed.filter_category');
        return $this->render('SportLobsterFeedBundle:Default:index.html.twig', compact('news', 'title', 'description', 'topic', 'filter_category'));
    }
}
