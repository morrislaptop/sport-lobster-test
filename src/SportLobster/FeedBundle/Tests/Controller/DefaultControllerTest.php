<?php

namespace SportLobster\FeedBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/football');

        // Test it displays channel details
        $this->assertTrue($crawler->filter('html:contains("Sky Sports | Football")')->count() > 0);

        // Test it gets at least one article
        $this->assertTrue($crawler->filter('html:contains("Benfica snatch the advantage")')->count() > 0);

        // Test no Preview articles are in there
        $this->assertFalse($crawler->filter('html:contains("Preview")')->count() > 0);
    }

    public function testReportCategory()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/football/Report');

        // Test that all categories are Report (not sure if this is the best way)
        $categories = $crawler->filter('strong.articleCategory')->extract(array('_text'));
        foreach ($categories as $category) {
        	$this->assertEquals($category, 'Report');
        }
    }
}
